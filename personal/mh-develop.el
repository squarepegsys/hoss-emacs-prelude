;; this make sure that Dired always works in MacOS
(setq insert-directory-program "gls" dired-use-ls-dired t)
(setq dired-listing-switches "-al --group-directories-first")

(prelude-require-packages '(company-tabnine))
(add-to-list 'company-backends #'company-tabnine)


;; on initial setup, run M-x company-tabnine-install-binary
;; Trigger completion immediately.
(setq company-idle-delay 0)

(prelude-require-packages '(blacken))


(add-hook 'python-mode-hook
          (lambda ()
            (blacken-mode 1)
            (anaconda-mode)
            ;; other config
            ))

(prelude-require-packages '(direnv))
(direnv-mode)

;(setq git-commit-style-convention-checks
;      (remove 'non-empty-second-line git-commit-style-convention-checks))
