(prelude-require-packages '(hyperbole ))

(hyperbole-mode 1)

(prelude-require-packages '(hydra ))

;; hyperbole in a python identifier doesn't see Anaconda Mode
;; so this advice overrides it's smart-python-tag behavior
(defun mh/override-smart-python-tag (original-function &rest args)
  "Conditionally override `smart-python-tag` to use `anaconda-mode-find-definitions` in Python mode."
  ;; Check if we're in python-mode and anaconda-mode is active
  (if (and (eq major-mode 'python-mode)
           (bound-and-true-p anaconda-mode))
      (call-interactively 'anaconda-mode-find-definitions) ;; Call anaconda-mode-find-definitions interactively
    (apply original-function args))) ;; Otherwise, call the original function

(advice-add 'smart-python-tag :around #'mh/override-smart-python-tag)

(defhydra mh/window-config-hydra (:color blue :hint nil)
  "
^Window Configurations^
-----------------------
_s_: Save Window Config  _y_: Yank Window Config
"
  ("s" hywconfig-ring-save "save" )
  ("y" hywconfig-yank-pop  "yank" :color red))

(transient-define-prefix mh/hyperbole-transient ()
  ["Hyperbole commands"
   ("s" "save window" hywconfig-ring-save)
   ("y" "yank window" hywconfig-yank-pop)
   ("w" "window control" hycontrol-enable-windows-mode)
   ("f" "frame control" hycontrol-enable-frames-mode)
   ])

(global-set-key (kbd "C-h j") 'mh/hyperbole-transient)
(global-set-key (kbd "C-c m") 'mh/window-config-hydra/body)

(defun mh/open-models-and-find-class (class-name)
  "Open models.py in the current directory and position the cursor at the specified CLASS-NAME."
  (interactive "sEnter class name: ")
  (let ((current-dir (file-name-directory (buffer-file-name)))
        models-path)
    (setq models-path (concat current-dir "models.py"))
    (when (file-exists-p models-path)
      (find-file-other-window models-path)
      (goto-char (point-min))
      (search-forward (concat "class " class-name "(") nil t))
    (unless (file-exists-p models-path)
      (message "models.py not found in the current directory"))))
(defun mh/dj-open-models-and-find-class (class-name)

  (mh/dj-open-models-and-find-class class-name)

  )
