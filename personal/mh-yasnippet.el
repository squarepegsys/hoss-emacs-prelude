;; from https://github.com/bruce/emacs-prelude/blob/master/personal/yasnippets.el

;;; yasnippet.el --- load snippets

;;; Commentary:
;; I love not typing as much. ¯\_(ツ)_/¯

;;; Code:

;; Get it!


(define-key yas-minor-mode-map [(tab)]     'yas-expand)
(define-key yas-minor-mode-map (kbd "TAB")     'yas-expand)
(define-key yas-minor-mode-map (kbd "<tab>")     'yas-expand)


(prelude-require-packages '(yasnippet
                            yasnippet-snippets))



;; More collections of snippets
(add-to-list 'yas-snippet-dirs "~/.emacs.d/personal/snippets")
(yas-global-mode 1)

;; disable yasnippet in magit
(defun mh/disable-yas-in-magit ()
  "Disable `yas-minor-mode' in Magit buffers."
  (when (derived-mode-p 'magit-mode)
    (yas-minor-mode -1)))

(add-hook 'magit-mode-hook 'mh/disable-yas-in-magit)


;;; yasnippet.el ends here
